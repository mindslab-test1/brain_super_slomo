# brain_super_slomo

### Download Pre-Trained file
`https://drive.google.com/file/d/1q8Fo5k-yWkoAzK_Pa6_IDOLbN4F_dIe2/view?usp=sharing`

###  Build Docker File
`docker build -t superslomo:0.0 .`

###  Run Docker Container
`docker run -itd --ipc host -v THIS_FOLDER:/workspace --name superslomo --gpus all --tag superslomo:0.0`

### Attach to Container
`docker attach superslomo`

### Inference the SuperSloMo Model
`python eval.py INPUT_VIDEO.mp4 --checkpoint SuperSloMo_73.ckpt --output OUTPUT_VIDEO.mp4 --scale 4 --fps 30 --gpunum 0`